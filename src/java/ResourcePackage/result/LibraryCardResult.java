
package ResourcePackage.result;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "LibraryCardResult")
public class LibraryCardResult {

    protected String id;
    protected String userName;
    protected String userSurname;
    protected String userID;
    protected BookList books;

    @XmlElement
    public String getID() {
        return id;
    }

    public void setID(String value) {
        this.id = value;
    }

    @XmlElement
    public String getUserName() {
        return userName;
    }

    public void setUserName(String value) {
        this.userName = value;
    }

    @XmlElement
    public String getUserSurname() {
        return userSurname;
    }

    public void setUserSurname(String value) {
        this.userSurname = value;
    }
    
    @XmlElement
    public String getUserID() {
        return userID;
    }

    public void setUserID(String value) {
        this.userID = value;
    }
    
    @XmlElement
    public BookList getBooks() {
        return books;
    }

    public void setBooks(BookList value) {
        this.books = value;
    }

}
