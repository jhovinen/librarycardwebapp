
package ResourcePackage.result;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "ActionResult")
public class ActionResult {

    protected LibraryCardResult libraryCard;
    protected String error;

    @XmlElement
    public LibraryCardResult getLibraryCard() {
        return libraryCard;
    }

    public void setLibraryCard(LibraryCardResult value) {
        this.libraryCard = value;
    }
    
    @XmlElement
    public String getError() {
        return error;
    }

    public void setError(String value) {
        this.error = value;
    }

}
