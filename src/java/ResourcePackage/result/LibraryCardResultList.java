
package ResourcePackage.result;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "LibraryCardResultList")
public class LibraryCardResultList {

    @XmlElement(required = true)
    protected List<LibraryCardResult> libraryCard;

    public List<LibraryCardResult> getLibraryCard() {
        if (libraryCard == null) {
            libraryCard = new ArrayList<>();
        }
        return this.libraryCard;
    }

}
