
package ResourcePackage.result;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "FindLibraryCardResponse")
public class FindLibraryCardResponse {

    protected LibraryCardResultList libraryCards;
    protected String error;

    @XmlElement
    public LibraryCardResultList getLibraryCards() {
        return libraryCards;
    }

    public void setLibraryCards(LibraryCardResultList value) {
        this.libraryCards = value;
    }
    
    @XmlElement
    public String getError() {
        return error;
    }

    public void setError(String value) {
        this.error = value;
    }

}
