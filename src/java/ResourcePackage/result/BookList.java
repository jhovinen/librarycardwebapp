
package ResourcePackage.result;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "BookList")
public class BookList {

    protected List<Book> book;

    @XmlElement
    public List<Book> getBook() {
        if (book == null) {
            book = new ArrayList<>();
        }
        return this.book;
    }

}
