
package ResourcePackage.result;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Book")
public class Book {

    protected String barcode;
    protected String name;
    protected String author;
    protected String owner;
    protected String dayOfReturn;
    protected String overview;

    @XmlElement
    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String value) {
        this.barcode = value;
    }

    @XmlElement
    public String getName() {
        return name;
    }

    public void setName(String value) {
        this.name = value;
    }

    @XmlElement
    public String getAuthor() {
        return author;
    }

    public void setAuthor(String value) {
        this.author = value;
    }

    @XmlElement
    public String getOwner() {
        return owner;
    }

    public void setOwner(String value) {
        this.owner = value;
    }

    @XmlElement
    public String getDayOfReturn() {
        return dayOfReturn;
    }

    public void setDayOfReturn(String value) {
        this.dayOfReturn = value;
    }

    @XmlElement
    public String getOverview() {
        return overview;
    }

    public void setOverview(String value) {
        this.overview = value;
    }

}
