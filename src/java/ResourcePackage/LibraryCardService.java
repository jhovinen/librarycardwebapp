/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ResourcePackage;

import ee.ttu.idu0075.library_card_service._1.ActionResult;
import ee.ttu.idu0075.library_card_service._1.AddBookRequest;
import ee.ttu.idu0075.library_card_service._1.AddLibraryCardRequest;
import ee.ttu.idu0075.library_card_service._1.Book;
import ee.ttu.idu0075.library_card_service._1.BookBarcodeList;
import ee.ttu.idu0075.library_card_service._1.BookList;
import ee.ttu.idu0075.library_card_service._1.ExtendTimeRequest;
import ee.ttu.idu0075.library_card_service._1.FindLibraryCardRequest;
import ee.ttu.idu0075.library_card_service._1.FindLibraryCardResponse;
import ee.ttu.idu0075.library_card_service._1.LibraryCard;
import ee.ttu.idu0075.library_card_service._1.LibraryCardList;
import ee.ttu.idu0075.library_card_service._1.LibraryCardResult;
import ee.ttu.idu0075.library_card_service._1.LibraryCardResultList;
import ee.ttu.idu0075.library_card_service._1.RemoveBookRequest;
import java.util.Optional;
import javax.jws.WebService;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 *
 * @author Julia
 */
@WebService(serviceName = "libraryCardService", portName = "libraryCardServicePort", endpointInterface = "ee.ttu.idu0075.wsdl.librarycardwebapp.java.library_card.LibraryCardServicePortType", targetNamespace = "http:/www.ttu.ee/idu0075/wsdl/LibraryCardWebApp/java/library-card", wsdlLocation = "WEB-INF/wsdl/LibraryCardService/library-card.wsdl")
public class LibraryCardService {
    
    private LibraryCardList libraryCards = new LibraryCardList();
    private final BookList books = new BookList();
    private int index = 0;
    private final String token = "secret";
    
    public LibraryCardService() {
        Book book1 = new Book();
        Book book2 = new Book();
 
        book1.setAuthor("Agata Christie");
        book1.setBarcode("b1");
        book1.setName("Murder on the Orient Express");
        book1.setOwner(null);
        book1.setDayOfReturn(null);
        
        book2.setAuthor("Agata Christie");
        book2.setBarcode("b2");
        book2.setName("Death on the Nile");
        book2.setOwner("u-1");
        book2.setDayOfReturn(null);

        books.getBook().add(book1);
        books.getBook().add(book2);
        
        BookBarcodeList barcodeList = new BookBarcodeList();
        barcodeList.getBookBarcode().add("b2");
        LibraryCard libraryCard = new LibraryCard();
        libraryCard.setID("u-1");
        libraryCard.setUserID("49011299999");
        libraryCard.setUserName("Keira");
        libraryCard.setUserSurname("Knightley");
        libraryCard.setBooks(barcodeList);
        libraryCards.getLibraryCard().add(libraryCard);
    }
    
    protected LibraryCardResult cloneResultFromLibraryCard ( LibraryCard libraryCard ) {
        LibraryCardResult libraryCardResult = new LibraryCardResult();
        libraryCardResult.setID(libraryCard.getID());
        libraryCardResult.setUserID(libraryCard.getUserID());
        libraryCardResult.setUserName(libraryCard.getUserName());
        libraryCardResult.setUserSurname(libraryCard.getUserSurname());
        libraryCardResult.setBooks(new BookList());
        libraryCard.getBooks().getBookBarcode().forEach((barcode) -> {
            books.getBook().stream().filter((book) -> (book.getBarcode().equals(barcode))).forEachOrdered((book) -> {
                libraryCardResult.getBooks().getBook().add(book);
            });
        });
        return libraryCardResult;
    }
    
    public ActionResult addLibraryCard(AddLibraryCardRequest parameters) {
        ActionResult actionResult = new ActionResult();
        if(!token.equals(parameters.getToken())){
            actionResult.setError("Error: do not have access.");
            return actionResult;
        }
            
        if(parameters.getUserID() != null && parameters.getUserName() != null && parameters.getUserSurname() != null &&
                parameters.getUserID().length() != 0 && parameters.getUserName().length() != 0 && parameters.getUserSurname().length() != 0){
            String newID = "user"+index;
            index++;
            
            LibraryCard libraryCard = new LibraryCard();
            libraryCard.setID(newID);
            libraryCard.setUserID(parameters.getUserID());
            libraryCard.setUserName(parameters.getUserName());
            libraryCard.setUserSurname(parameters.getUserSurname());
            libraryCard.setBooks(new BookBarcodeList());
            libraryCards.getLibraryCard().add(libraryCard);
            actionResult.setLibraryCard(cloneResultFromLibraryCard(libraryCard));
        } else {
            actionResult.setError("Error: some input data is missing.");
        }
        return actionResult;
    }

    public FindLibraryCardResponse findLibraryCard(FindLibraryCardRequest parameters) throws DatatypeConfigurationException {
        FindLibraryCardResponse findLibraryCardResponse = new FindLibraryCardResponse();
        if(!token.equals(parameters.getToken())){
            findLibraryCardResponse.setError("Error: do not have access.");
            return findLibraryCardResponse;
        }
        boolean isId = parameters.getID() != null && parameters.getID().length() != 0;
        boolean isUserID = parameters.getUserID() != null && parameters.getUserID().length() != 0;
        boolean isName = parameters.getUserName() != null && parameters.getUserName().length() != 0;
        boolean isSurname = parameters.getUserSurname() != null && parameters.getUserSurname().length() != 0;
        
         
        LibraryCardResultList libraryCardResultList = new LibraryCardResultList();
        findLibraryCardResponse.setLibraryCards(libraryCardResultList);
        
        for(LibraryCard libraryCard: libraryCards.getLibraryCard()){
            if((isId && libraryCard.getID().equals(parameters.getID())) || !isId){
                if((isUserID && libraryCard.getUserID().equals(parameters.getUserID())) || !isUserID){
                    if(((isName && libraryCard.getUserName().equals(parameters.getUserName())) || !isName)){
                        if((isSurname && libraryCard.getUserSurname().equals(parameters.getUserSurname())) || !isSurname){
                            libraryCardResultList.getLibraryCard().add(cloneResultFromLibraryCard(libraryCard));
                        }
                    }
                }
            }
        }
        
        return findLibraryCardResponse;
    }

    public ActionResult addBook(AddBookRequest parameters) throws DatatypeConfigurationException {
        ActionResult actionResult = new ActionResult();
        if(!token.equals(parameters.getToken())){
            actionResult.setError("Error: do not have access.");
            return actionResult;
        }
        if (parameters.getLibraryCardID() == null || parameters.getLibraryCardID().length() == 0 || parameters.getBookBarcodeList() == null || 
                parameters.getDate() == null || !(parameters.getDate() instanceof XMLGregorianCalendar)) {
            actionResult.setError("Error: input data is missing or has wrong format.");
            return actionResult;
        }
        
        BookBarcodeList selectedBookList;
        LibraryCard selectedLibraryCard;
        Optional<LibraryCard> libraryCardSearchResult = libraryCards.getLibraryCard().stream().filter((LibraryCard libraryCard) -> (libraryCard.getID().equals(parameters.getLibraryCardID()))).findFirst();
        if (libraryCardSearchResult.isPresent()) {
            selectedLibraryCard = libraryCardSearchResult.get();
        } else {
            actionResult.setError("Error: user not found.");
            return actionResult;
        }
        
        selectedBookList = selectedLibraryCard.getBooks();
        for(String selectedBarcode: parameters.getBookBarcodeList().getBookBarcode()){
            for(Book book: books.getBook()){
                if(book.getBarcode().equals(selectedBarcode)){
                    if(book.getOwner() != null) {
                        actionResult.setError("Error: Book nr." + book.getBarcode() + " is busy. Owner - " + book.getOwner());
                    } else {
                        selectedBookList.getBookBarcode().add(book.getBarcode());
                        book.setOwner(selectedLibraryCard.getID());
                        book.setDayOfReturn(parameters.getDate());
                    }
                }
            }
        }
        
        actionResult.setLibraryCard(cloneResultFromLibraryCard(selectedLibraryCard));
        return actionResult;
    }

    public ActionResult extendTime(ExtendTimeRequest parameters) throws DatatypeConfigurationException {
        ActionResult actionResult = new ActionResult();
        if(!token.equals(parameters.getToken())){
            actionResult.setError("Error: do not have access.");
            return actionResult;
        }
        
        if(parameters.getLibraryCardID() == null || parameters.getBookBarcodeList() == null || parameters.getDate() == null || !(parameters.getDate() instanceof XMLGregorianCalendar)){
            actionResult.setError("Error: input data is missing or has wrong format.");
            return actionResult;
        }
        
        BookBarcodeList selectedBookList;
        LibraryCard selectedLibraryCard;
        Optional<LibraryCard> libraryCardSearchResult = libraryCards.getLibraryCard().stream().filter((LibraryCard libraryCard) -> (libraryCard.getID().equals(parameters.getLibraryCardID()))).findFirst();
        if (libraryCardSearchResult.isPresent()) {
            selectedLibraryCard = libraryCardSearchResult.get();
        } else {
            actionResult.setError("Error: user not found.");
            return actionResult;
        }
        
        selectedBookList = selectedLibraryCard.getBooks();
        parameters.getBookBarcodeList().getBookBarcode().forEach((selectedBarcode) -> {
            books.getBook().stream().filter((book) -> (book.getBarcode().equals(selectedBarcode))).forEachOrdered((book) -> {
                book.setDayOfReturn(parameters.getDate());
            });
        });
        
        actionResult.setLibraryCard(cloneResultFromLibraryCard(selectedLibraryCard));
        return actionResult;
    }

    public ActionResult removeBook(RemoveBookRequest parameters) {
        ActionResult actionResult = new ActionResult();
        if(!token.equals(parameters.getToken())){
            actionResult.setError("Error: do not have access.");
            return actionResult;
        }
        
        if(parameters.getLibraryCardID() == null || parameters.getBookBarcode()== null){
            actionResult.setError("Error: input data is missing.");
            return actionResult;
        }
        
        BookBarcodeList selectedBookList;
        String selectedBarcode;
        LibraryCard selectedLibraryCard;
        Optional<LibraryCard> libraryCardSearchResult = libraryCards.getLibraryCard().stream().filter((LibraryCard libraryCard) -> (libraryCard.getID().equals(parameters.getLibraryCardID()))).findFirst();
        if (libraryCardSearchResult.isPresent()) {
            selectedLibraryCard = libraryCardSearchResult.get();
        } else {
            actionResult.setError("Error: user not found.");
            return actionResult;
        }
        
        selectedBookList = selectedLibraryCard.getBooks();
        selectedBarcode = parameters.getBookBarcode();
        Optional<Book> bookSearchResult;

        bookSearchResult = books.getBook().stream().filter((Book book) -> (book.getBarcode().equals(selectedBarcode))).findFirst();
        if (bookSearchResult.isPresent()) 
            selectedBookList.getBookBarcode().remove(bookSearchResult.get().getBarcode());
            books.getBook().stream().filter((book) -> (book.getBarcode().equals(selectedBarcode))).map((book) -> {
                book.setOwner(null);
                return book;
            }).forEachOrdered((book) -> {
                book.setDayOfReturn(null);
            });

        
        actionResult.setLibraryCard(cloneResultFromLibraryCard(selectedLibraryCard));
        return actionResult;
    }
    
}
