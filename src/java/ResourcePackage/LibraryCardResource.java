/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ResourcePackage;

import ResourcePackage.result.ActionResult;
import ResourcePackage.result.Book;
import ee.ttu.idu0075.library_card_service._1.BookBarcodeList;
import ResourcePackage.result.BookList;
import ResourcePackage.result.FindLibraryCardResponse;
import ee.ttu.idu0075.library_card_service._1.LibraryCard;
import ee.ttu.idu0075.library_card_service._1.LibraryCardList;
import ResourcePackage.result.LibraryCardResult;
import ResourcePackage.result.LibraryCardResultList;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 * REST Web Service
 *
 * @author Julia
 */
@Path("libraryCard")
public class LibraryCardResource {

    @Context
    private UriInfo context;
   
    private LibraryCardList libraryCards = new LibraryCardList();
    private final BookList books = new BookList();
    private int index = 0;
    private final String token = "secret";
    
    public LibraryCardResource() {
        Book book1 = new Book();
        Book book2 = new Book();
 
        book1.setAuthor("Agata Christie");
        book1.setBarcode("b1");
        book1.setName("Murder on the Orient Express");
        book1.setOwner(null);
        book1.setDayOfReturn(null);
        
        book2.setAuthor("Agata Christie");
        book2.setBarcode("b2");
        book2.setName("Death on the Nile");
        book2.setOwner("u-1");
        book1.setDayOfReturn(null);

        books.getBook().add(book1);
        books.getBook().add(book2);
        
        BookBarcodeList barcodeList = new BookBarcodeList();
        barcodeList.getBookBarcode().add("b2");
        LibraryCard libraryCard = new LibraryCard();
        libraryCard.setID("u-1");
        libraryCard.setUserID("49011299999");
        libraryCard.setUserName("Keira");
        libraryCard.setUserSurname("Knightley");
        libraryCard.setBooks(barcodeList);
        libraryCards.getLibraryCard().add(libraryCard);
    }
    
    protected LibraryCardResult cloneResultFromLibraryCard ( LibraryCard libraryCard ) {
        LibraryCardResult libraryCardResult = new LibraryCardResult();
        libraryCardResult.setID(libraryCard.getID());
        libraryCardResult.setUserID(libraryCard.getUserID());
        libraryCardResult.setUserName(libraryCard.getUserName());
        libraryCardResult.setUserSurname(libraryCard.getUserSurname());
        libraryCardResult.setBooks(new BookList());
        for(String barcode:  libraryCard.getBooks().getBookBarcode()){
            for(Book book : books.getBook()){
                if(book.getBarcode().equals(barcode))
                    libraryCardResult.getBooks().getBook().add(book);
            }
        }
        return libraryCardResult;
    }
    
    protected XMLGregorianCalendar convertStringToDate(String string){
        try {
            XMLGregorianCalendar date = DatatypeFactory.newInstance().newXMLGregorianCalendar(string);
            return date;
            
        }catch(Exception e){
            return null;
        }
    }

    @POST
    @Path("addLibraryCard")
    @Produces("application/json")
    @Consumes("application/json")
    public ActionResult addLibraryCard(@QueryParam("id") String id, 
            @QueryParam("name") String name, 
            @QueryParam("surname") String surname, 
            @QueryParam("token") String tokenResult){
        ActionResult actionResult = new ActionResult();
        if(!token.equals(tokenResult)){
            actionResult.setError("Error: do not have access.");
            return actionResult;
        }
        if(id != null && name != null && surname != null){
            String newID = "user"+index;
            index++;
            
            LibraryCard libraryCard = new LibraryCard();
            libraryCard.setID(newID);
            libraryCard.setUserID(id);
            libraryCard.setUserName(name);
            libraryCard.setUserSurname(surname);
            libraryCard.setBooks(new BookBarcodeList());
            libraryCards.getLibraryCard().add(libraryCard);
            actionResult.setLibraryCard(cloneResultFromLibraryCard(libraryCard));
        } else {
            actionResult.setError("Error: some input data is missing.");
        }
        return actionResult;
    }
    

    @GET
    @Produces("application/json")
    public FindLibraryCardResponse findLibraryCard(@QueryParam("id") String id,
            @QueryParam("userId") String userId, @QueryParam("name") String name,
            @QueryParam("surname") String surname, @QueryParam("token") String tokenResult) {
        FindLibraryCardResponse findLibraryCardResponse = new FindLibraryCardResponse(); 
        if(!token.equals(tokenResult)){
            findLibraryCardResponse.setError("Error: do not have access.");
            return findLibraryCardResponse;
        }
        boolean isId = id != null && id.length() != 0;
        boolean isUserID = userId != null && userId.length() != 0;
        boolean isName = name != null && name.length() != 0;
        boolean isSurname = surname != null && surname.length() != 0;

        LibraryCardResultList libraryCardResultList = new LibraryCardResultList();
        findLibraryCardResponse.setLibraryCards(libraryCardResultList);
        
        for(LibraryCard libraryCard: libraryCards.getLibraryCard()){
            if((isId && libraryCard.getID().equals(id)) || !isId){
                if((isUserID && libraryCard.getUserID().equals(userId)) || !isUserID){
                    if(((isName && libraryCard.getUserName().equals(name)) || !isName)){
                        if((isSurname && libraryCard.getUserSurname().equals(surname)) || !isSurname){
                            libraryCardResultList.getLibraryCard().add(cloneResultFromLibraryCard(libraryCard));
                        }
                    }
                }
            }
        }
 
        return findLibraryCardResponse;
    }
    
    @POST
    @Path("addBook")
    @Produces("application/json")
    public ActionResult addBook(@QueryParam("id") String libraryCardId, 
            @QueryParam("barcodeList") String barcodeArrayString,
            @QueryParam("date") String stringDate, 
            @QueryParam("token") String tokenResult) {
        ActionResult actionResult = new ActionResult();
        if (libraryCardId == null || barcodeArrayString == null || stringDate == null) {
            actionResult.setError("Error: input data is missing");
            return actionResult;
        }
        
        XMLGregorianCalendar date = convertStringToDate(stringDate);
        String[] barcodeArray = barcodeArrayString.split(",");

        if (barcodeArray == null || barcodeArray.length <= 0 || date == null || !(date instanceof XMLGregorianCalendar)) {
            actionResult.setError("Error: input data has wrong format.");
            return actionResult;
        }
        
        BookBarcodeList selectedBookList;
        LibraryCard selectedLibraryCard;
        LibraryCard libraryCardSearchResult = null;
        for(LibraryCard libraryCard :libraryCards.getLibraryCard()){
            if(libraryCard.getID().equals(libraryCardId)){
                libraryCardSearchResult = libraryCard;
                break;
            }
        }
        if (libraryCardSearchResult != null) {
            selectedLibraryCard = libraryCardSearchResult;
        } else {
            actionResult.setError("Error: user not found.");
            return actionResult;
        }
        
        selectedBookList = selectedLibraryCard.getBooks();
        for(String selectedBarcode: barcodeArray){
            for(Book book: books.getBook()){
                    if(book.getOwner() != null) {
                        actionResult.setError("Error: Book nr." + book.getBarcode() + " is busy. Owner - " + book.getOwner());
                    } else {
                        selectedBookList.getBookBarcode().add(book.getBarcode());
                        book.setOwner(selectedLibraryCard.getID());
                        book.setDayOfReturn(date.toString());
                    }
            }
        }
        actionResult.setLibraryCard(cloneResultFromLibraryCard(selectedLibraryCard));
        return actionResult;
    };

    @PUT
    @Path("extendTime")
    @Produces("application/json")
    @Consumes("application/json")
    public ActionResult extendTime(@QueryParam("id") String libraryCardId, 
            @QueryParam("barcodeList") String barcodeArrayString,
            @QueryParam("date") String stringDate, 
            @QueryParam("token") String tokenResult) throws DatatypeConfigurationException {
                ActionResult actionResult = new ActionResult();
        if (libraryCardId == null || barcodeArrayString == null || stringDate == null) {
            actionResult.setError("Error: input data is missing");
            return actionResult;
        }
        
        XMLGregorianCalendar date = convertStringToDate(stringDate);
        String[] barcodeArray = barcodeArrayString.split(",");

        if (barcodeArray == null || barcodeArray.length <= 0 || date == null || !(date instanceof XMLGregorianCalendar)) {
            actionResult.setError("Error: input data has wrong format.");
            return actionResult;
        }
        
        LibraryCard selectedLibraryCard;
        LibraryCard libraryCardSearchResult = null;
        for(LibraryCard libraryCard :libraryCards.getLibraryCard()){
            if(libraryCard.getID().equals(libraryCardId)){
                libraryCardSearchResult = libraryCard;
                break;
            }
        }
        if (libraryCardSearchResult != null) {
            selectedLibraryCard = libraryCardSearchResult;
        } else {
            actionResult.setError("Error: user not found.");
            return actionResult;
        }
        
        BookBarcodeList selectedBookList = selectedLibraryCard.getBooks();
        for(String selectedBarcode: barcodeArray){
            for(Book book: books.getBook()){
                if(book.getBarcode().equals(selectedBarcode)){
                    book.setDayOfReturn(date.toString());
                }
            }
        }
        actionResult.setLibraryCard(cloneResultFromLibraryCard(selectedLibraryCard));
        return actionResult;
    }

    @DELETE
    @Path("removeBook")
    @Produces("application/json")
    @Consumes("application/json")
    public ActionResult removeBook(@QueryParam("id") String libraryCardId, 
            @QueryParam("barcode") String bookBarcode, 
            @QueryParam("token") String tokenResult) {
        ActionResult actionResult = new ActionResult();
        
        if(libraryCardId == null || bookBarcode== null){
            actionResult.setError("Error: input data is missing.");
            return actionResult;
        }
 
        LibraryCard selectedLibraryCard;
        LibraryCard libraryCardSearchResult = null;
        for(LibraryCard libraryCard :libraryCards.getLibraryCard()){
            if(libraryCard.getID().equals(libraryCardId)){
                libraryCardSearchResult = libraryCard;
                break;
            }
        }
        if (libraryCardSearchResult != null) {
            selectedLibraryCard = libraryCardSearchResult;
        } else {
            actionResult.setError("Error: user not found.");
            return actionResult;
        }
        
        BookBarcodeList selectedBookList = selectedLibraryCard.getBooks();
        Book bookSearchResult = null;

        for(Book book: books.getBook()){
            if(book.getBarcode().equals(bookBarcode)){
                bookSearchResult = book;
                break;
            }
        }
        if (bookSearchResult != null) 
            selectedBookList.getBookBarcode().remove(bookSearchResult.getBarcode());
            for(Book book: books.getBook()){
                if(book.getBarcode().equals(bookBarcode)){
                    book.setOwner(null);
                    book.setDayOfReturn(null);
                }
            }
        
        actionResult.setLibraryCard(cloneResultFromLibraryCard(selectedLibraryCard));
        return actionResult;
    }
    
}
