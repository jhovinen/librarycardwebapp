package ResourcePackage;

import java.util.Set;
import javax.ws.rs.core.Application;

@javax.ws.rs.ApplicationPath("webresources")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
//        try {
//            Class jsonProvider = Class.forName("org.glassFish.jersey.jackson.JacksonFeature");
//            resources.add(jsonProvider);
//        } catch (ClassNotFoundException ex){
//            java.util.logging.Logger.getLogger(getClass().getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
        addRestResourceClasses(resources);
        return resources;
    }
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(ResourcePackage.LibraryCardResource.class);
    }
}
